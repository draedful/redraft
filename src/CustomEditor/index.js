import React from 'react';
import { connect } from 'react-redux';
import { setDocument, handleKeyCommand, setDecorators } from 'src/editor/actions';
import Editor from './editor';

export default connect(
	(editorState) => ({ editorState }),
	(dispatch) => ({
		setDecorators: (decorators) => dispatch(setDecorators(decorators)),
		handleKeyCommand: (command, ...args) => dispatch(handleKeyCommand(command, args)),
		onTab: () => console.log('onTab'),
		onEscape: () => console.log('onEscape'),
		onUpArrow: () => console.log('onUpArrow'),
		onDownArrow: () => console.log('onDownArrow'),
		onChange: (editor) => dispatch(setDocument(editor))
	})
)(Editor);
