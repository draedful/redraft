// @flow
import type { ContentBlock } from 'draft-js';
import React from 'react';
import PropTypes from 'prop-types';
import { Editor } from 'draft-js';
import { getEntity, isAtomicBlock, getAtomicEntity } from './helpers';

import EntityResolver from './EntityResolver';


export default class CustomEditor extends React.Component {

	static childContextTypes = {
		entityResolver: PropTypes.instanceOf(EntityResolver)
	};

	getChildContext() {
		return {
			entityResolver: this.entityResolver
		}
	}

	getEditorState() {
		return this.props.editorState;
	}

	componentDidMount() {
		this.props.setDecorators(this.entityResolver.getRules());
	}

	constructor(props) {
		super(props);
		this.entityResolver = new EntityResolver();

		this.getBlockComponent = (block: ContentBlock) => {
			if (isAtomicBlock(block)) {
				const atomicEntity = getAtomicEntity(block);
				const entity = getEntity(this.props.editorState, atomicEntity);
				if (entity) {
					return this.entityResolver.getBlock(entity.getType());
				}
			}
		};

	}

	render() {
		return (
			<div>
				{this.props.children}
				<Editor
					{...this.props}
					blockRendererFn={this.getBlockComponent}
				/>
			</div>
		)
	}

}