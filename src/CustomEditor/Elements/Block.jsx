// @flow
import type { Element } from 'react';
import React from 'react';
import PropTypes from 'prop-types';
import EntityResolver from '../EntityResolver';

export default class Entity extends React.Component {

	static contextTypes = {
		entityResolver: PropTypes.instanceOf(EntityResolver)
	};

	static defaultProps = {
		editable: false
	};

	props: {
		name: |string,
		component: |Element,
		editable: ?boolean
	};

	componentWillMount() {
		this.context.entityResolver.setBlockComponent(this.props.name, this.props.component)
	}

	componentWillUnmount() {
		this.context.entityResolver.deleteBlock(this.props.name)
	}

	render() {
		return null;
	}

}