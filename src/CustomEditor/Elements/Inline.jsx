// @flow
import type { Element } from 'react';

import React from 'react';
import PropTypes from 'prop-types';
import EntityResolver from '../EntityResolver';
import { RedraftInlineEntityStrategy } from '../EntityResolver';

export default class Inline extends React.Component {

	static contextTypes = {
		entityResolver: PropTypes.instanceOf(EntityResolver)
	};

	static defaultProps = {
		editable: true,
		strategy: null
	};

	props: {
		name: string,
		component: Element,
		editable: ?boolean,
		strategy: ?RedraftInlineEntityStrategy
	};

	shouldComponentUpdate() {
		return this.props.shouldComponentUpdate || false;
	}

	componentWillMount() {
		this.context.entityResolver.setInlineComponent(this.props.name, this.props.component);
		if(this.props.strategy) {
			this.context.entityResolver.setStrategy(this.props.name, this.props.strategy);
		}
	}

	componentWillUnmount() {
		this.context.entityResolver.deleteInline(this.props.name)
	}

	render() {
		return null;
	}

}