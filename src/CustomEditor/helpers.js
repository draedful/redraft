//@flow
import type { EntityInstance, ContentBlock } from 'draft-js';
import { getCurrentContent } from '../../src/src/editor/selectors';

export const isAtomicBlock = (block: ContentBlock): boolean => block.getType() === 'atomic';

export const getAtomicEntity = (block: ContentBlock): number => block.getEntityAt(0);

export const getEntity = (editorState: EditorState, entityId: number): EntityInstance | null => {
	return getCurrentContent(editorState).getEntity(entityId)
};