// @flow
import type { ContentBlock } from 'draft-js';
import type { DraftDecorator } from 'draft-js/lib/DraftDecorator';
import type { DraftDecoratorStrategy } from 'draft-js/lib/DraftDecoratorStrategy';
import type { Element } from 'react';
import { Entity } from 'draft-js';
import React from 'react';

export type RedraftInlineEntityStrategy = RegExp | DraftDecoratorStrategy

declare type EntityConfig = {
	component: Element,
	editable: boolean,
};

declare interface EntityStorage {
	inlineEntities: Map<string, EntityConfig>,
	atomicEntities: Map<string, EntityConfig>,
	setInlineComponent(name: string, component: Element): EntityStorage,
	setBlockComponent(name: string, component: Element): EntityStorage,
	deleteBlock(name: string): EntityStorage,
	deleteInline(name: string): EntityStorage
}


const decorateComponent = (component) => {
	return (props) => {
		debugger;
		return React.createElement(component, props);
	}
};

export default class EntityResolver implements EntityStorage {

	constructor() {
		this.inlineEntities = new Map();
		this.atomicEntities = new Map();
		this.rules = new Map();

		const FindEntityStrategy = (contentBlock: ContentBlock, callback: Function) => {
			contentBlock.findEntityRanges(
				(character) => {
					const key = character.getEntity();
					if (key) {
						const type = Entity.get(key).getType();
						return this.inlineEntities.has(type);
					}
					return false;
				},
				callback
			)
		};

		const getEntityComponent = (props) => {
			const { entityKey, contentState } = props;
			const entity = contentState.getEntity(entityKey);
			const data = entity.getData();
			const entityType = entity.getType();
			const component = this.inlineEntities.get(entityType);
			if (component) {
				return React.createElement(component.component, {
					...props,
					data,
				})
			}
		};

		this.baseDecorator = {
			strategy: FindEntityStrategy,
			component: getEntityComponent
		};
	}

	getRules(): Array<DraftDecorator> {
		const rules = [this.baseDecorator];

		this.rules.forEach((strategy, name) => {
			rules.push({
				strategy,
				component: decorateComponent(this.inlineEntities.get(name))
			})
		});

		return rules;
	}

	setInlineComponent(name: string, component: Element): EntityResolver {
		this.inlineEntities.set(name, component);
		return this;
	}

	setBlockComponent(name: string, component: Element) {
		this.atomicEntities.set(name, component);
		return this;
	}

	deleteBlock(name) {
		this.atomicEntities.delete(name);
		return this;
	}

	deleteInline(name) {
		this.inlineEntities.delete(name);
		return this;
	}

	getBlock(name) {
		return this.atomicEntities.get(name);
	}

	getInline(name) {
		return this.inlineEntities.get(name);
	}

	setStrategy(name, strategyFn) {
		this.rules.set(name, strategyFn);
	}

}