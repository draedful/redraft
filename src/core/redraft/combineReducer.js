import reduce from 'lodash/reduce';

export default (...reducers) => (state= {}, action) => {
		return reduce(reducers, (prevState, reducer) => {
			return reducer(prevState, action);
		}, state);
};