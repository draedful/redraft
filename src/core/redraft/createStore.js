import { EditorState } from 'draft-js';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import combineReducer from './combineReducer';

export default function createDocument(reducer, enhances) {
	return createStore(combineReducer(reducer), EditorState.createEmpty(), applyMiddleware(thunk, enhances));
}