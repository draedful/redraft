// @flow
import { applyMiddleware } from 'redux';
import perflogger from 'redux-perf-middleware';
import createStore from './core/redraft/createStore';
import combineReducer from './core/redraft/combineReducer';

import EditorReducer from './src/editor';

window.restore = createStore(combineReducer(EditorReducer), applyMiddleware(perflogger));