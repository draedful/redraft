import { handleKeyCommand } from 'src/editor/actions';

export const BOLD = 'bold';
export const toBold = () => (dispatch) => dispatch(handleKeyCommand(BOLD));

export const ITALIC = 'italic';
export const toItalic = () => (dispatch) => dispatch(handleKeyCommand(ITALIC));