// @flow
import type { ContentState } from 'draft-js';
import type { DraftDecorator } from 'draft-js/lib/DraftDecorator'
import { createReducer } from 'redux-act';
import { EditorState, CompositeDecorator } from 'draft-js';
import * as Action from './actions';


export default createReducer({
	[Action.clearDocument]: () => EditorState.createEmpty(document),
	[Action.setDocument]: (currentEditor, newEditor) => newEditor,
	[Action.setContentState]: (editor: ?EditorState, newContentState: ContentState) => EditorState.push(editor, newContentState),
	[Action.setSelectionState]: (editor: ?EditorState, payload) => {
		const { selection, isForce } = payload;
		return isForce ? EditorState.forceSelection(editor, selection) : EditorState.acceptSelection(editor, selection);
	},
	[Action.setDecorators]: (editor: EditorState, decorators: Array<DraftDecorator>) => {
		return EditorState.set(editor, { decorator: new CompositeDecorator(decorators) })
	}
});