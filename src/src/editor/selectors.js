// @flow
import type { EditorState, ContentState, SelectionState } from 'draft-js';
import type { RawDraftContentState } from 'draft-js/lib/RawDraftContentState';

import { convertToRaw, getVisibleSelectionRect } from 'draft-js';


export const getSelection = (editorState: EditorState): SelectionState => editorState.getSelection();

export const getCurrentContent = (editorState: EditorState): ContentState  => editorState.getCurrentContent();

export const getRawContent = (editorState: EditorState): RawDraftContentState => convertToRaw(getCurrentContent(editorState));

export const getSelectionRect = () => getVisibleSelectionRect(window);