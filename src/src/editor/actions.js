// @flow
import type { SelectionState } from 'draft-js';
import type { Dispatch, Action } from 'redux';
import { RichUtils } from 'draft-js';
import { createAction } from 'redux-act';
import { EditorState } from 'draft-js';

export const CLEAR_DOCUMENT = '@@redraft/CLEAR_DOCUMENT';
export const clearDocument = createAction(CLEAR_DOCUMENT);

export const SET_DOCUMENT = '@@redraft/SET_DOCUMENT';
export const setDocument = createAction(SET_DOCUMENT, (document) => {
	if (document instanceof EditorState) {
		return document;
	}
	return EditorState.createWithContent(document);
});

export const SET_CONTENT_STATE = '@@redraft/SET_CONTENT_STATE';
export const setContentState = createAction(SET_CONTENT_STATE);

export const SET_SELECTION_STATE = '@@redraft/SET_SELECTION_STATE';
export const setSelectionState = createAction(SET_SELECTION_STATE, (selection: SelectionState, isForce = true) => ({
	selection,
	isForce
}));

export const SET_DECORATORS = '@@redraft/SET_DECORATORS';
export const setDecorators = createAction(SET_DECORATORS);

export const handleKeyCommand = (command: string, args: Array<any>) => (dispatch: Dispatch<Action>, getEditor) => {
	const editor = RichUtils.handleKeyCommand(getEditor(), command);
	if(editor) {
		dispatch(setDocument(editor));
	}
};
