// @flow
import { EditorState } from 'draft-js';

import { createEmptyEditor, createFromText, createContentWithContent, createSelection } from './mocks';
import Reducer from '../reducer';
import * as Actions from '../actions';
import * as Selectors from '../selectors';

describe('Editor reducer', () => {

	it('should create new EditorState from exiting RawDocument', () => {
		const content = '123456';

		const newStore = Reducer(null, Actions.setDocument(createFromText(content)));
		expect(newStore).toBeInstanceOf(EditorState);

		const currentContent = Selectors.getCurrentContent(newStore);
		expect(currentContent.getPlainText()).toBe(content);
	});

	it('should return new cleared EditorState', () => {
		const newStore = Reducer(null, Actions.clearDocument());
		expect(newStore).toBeInstanceOf(EditorState);

		const currentContent = Selectors.getCurrentContent(newStore);
		expect(currentContent.getPlainText().length).toBe(0);
	});

	it('should return EditorState with new content', () => {
		const content = '123';
		const newStore = Reducer(createEmptyEditor(), Actions.setContentState(createContentWithContent(content)));
		expect(newStore).toBeInstanceOf(EditorState);

		const currentContent = Selectors.getCurrentContent(newStore);
		expect(currentContent.getPlainText()).toBe(content);
	});


	it('should return EditorState with new selection', () => {
		const editor = createEmptyEditor();
		const selection = createSelection(editor.getCurrentContent().getFirstBlock().getKey());
		const newStore = Reducer(editor, Actions.setSelectionState(selection, false));
		expect(newStore).toBeInstanceOf(EditorState);

		const selectionState = Selectors.getSelection(newStore);
		expect(selectionState).toBe(selection)
	});

	it('should be force accept selection ot EditorState', () => {
		const editor = createEmptyEditor();
		const selection = createSelection(editor.getCurrentContent().getFirstBlock().getKey());
		const newStore = Reducer(editor, Actions.setSelectionState(selection));
		expect(newStore).toBeInstanceOf(EditorState);

		const selectionState = Selectors.getSelection(newStore);
		expect(selectionState.getStartKey()).toBe(selection.getStartKey());
		expect(selectionState.getEndKey()).toBe(selection.getEndKey());
		expect(selectionState.getStartOffset()).toBe(selection.getStartOffset());
		expect(selectionState.getEndOffset()).toBe(selection.getEndOffset());
		expect(selectionState.getHasFocus()).not.toBe(selection.getHasFocus()); // Потому что использовался метод forceUpdate
	})

});