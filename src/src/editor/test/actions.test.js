// @flow
import type { Action } from 'redux';

import { EditorState, ContentState } from 'draft-js';
import * as Actions from '../actions';
import { createSelection } from './mocks';

const EmptyDocument = EditorState.createEmpty();

const expectActionType = (action: Action, type: string) => expect(action.type.includes(type)).toBe(true);

describe('Editor Actions', () => {

	it('create action for clearing document', () => {
		expectActionType(Actions.clearDocument(), Actions.CLEAR_DOCUMENT);
	});

	it('create action for setting new content to exiting editor', () => {
		expectActionType(Actions.setContentState(EditorState.createEmpty().getCurrentContent()), Actions.SET_CONTENT_STATE);
	});

	it('create action for force setting new selection to exiting editor', () => {
		const selection = createSelection('123');
		const action = Actions.setSelectionState(selection);
		expectActionType(action, Actions.SET_SELECTION_STATE);
		expect(action.payload).toBeDefined();
		expect(action.payload.selection).toBeDefined();
		expect(action.payload.isForce).toBeTruthy();
	});

	it('create action for setting new selection to exiting editor', () => {
		const selection = createSelection('123');
		const action = Actions.setSelectionState(selection, false);
		expectActionType(action, Actions.SET_SELECTION_STATE);
		expect(action.payload).toBeDefined();
		expect(action.payload.selection).toBeDefined();
		expect(action.payload.isForce).toBeFalsy();
	});

	describe('ForceUpdate document', () => {
		it('should be create action with for forceUpdate', () => {
			const action: Action = Actions.setDocument(EmptyDocument);
			expectActionType(action, Actions.SET_DOCUMENT);
			expect(action.payload).toBe(EmptyDocument)
		});
		it('should be create action with for forceUpdate', () => {
			const action: Action = Actions.setDocument(ContentState.createFromText(''));
			expectActionType(action, Actions.SET_DOCUMENT);
			expect(action.payload).toBeInstanceOf(EditorState);
		});
	});


});