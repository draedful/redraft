// @flow

import * as Selectors  from '../selectors';
import { createEmptyEditor } from './mocks';


describe('Editor Selectors', () => {

	it('should return current content of the editor', () => {

		const editor = createEmptyEditor();
		const currentContent = Selectors.getCurrentContent(editor);
		expect(currentContent).toBe(editor.getCurrentContent());

	});

	it('should return selection state of the editor', () => {
		const editor = createEmptyEditor();
		const currentContent = Selectors.getSelection(editor);
		expect(currentContent).toBe(editor.getSelection());
	});

	it('should return raw content of the editor', () => {
		const editor = createEmptyEditor();
		const rawContent = Selectors.getRawContent(editor);
		expect(rawContent.blocks).toBeInstanceOf(Array);
		expect(rawContent.entityMap).toBeInstanceOf(Object);
	})

});