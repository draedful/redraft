// @flow

import { EditorState, ContentState, SelectionState } from 'draft-js';

export const createEmptyEditor = (): EditorState => EditorState.createEmpty();

export const createFromText = (content: string): EditorState => {
	return EditorState.createWithContent(ContentState.createFromText(content));
};

export const createContentWithContent = (content: string): ContentState => ContentState.createFromText(content);


export const createSelection = (key: string) => SelectionState.createEmpty(key);