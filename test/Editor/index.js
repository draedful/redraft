import React from 'react';
import Link from './Entityes/Link';
import Editor from '../../src/CustomEditor';

export default (props) => {
	return <Editor {...props}>
		<Link />
	</Editor>
};