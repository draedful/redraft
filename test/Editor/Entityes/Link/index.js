import React from 'react';
import linkifyIt from 'linkify-it';
import tlds from 'tlds';
import Inline from '../../../../src/CustomEditor/Elements/Inline';
import LinkComponent from '../../Components/Link';
import { LinkEntityName } from './constants';

const linkify = linkifyIt();
linkify.tlds(tlds);

const linkStrategy = (contentBlock: Object, callback: Function) => {
	const links = linkify.match(contentBlock.get('text'));
	if (typeof links !== 'undefined' && links !== null) {
		for (let i = 0; i < links.length; i += 1) {
			callback(links[i].index, links[i].lastIndex);
		}
	}
};

export default () => {
	return <Inline
		component={LinkComponent}
		name={LinkEntityName}
		strategy={linkStrategy}
	/>
};