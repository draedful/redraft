// @flow
import React from 'react';


export default class PreRender extends React.Component {

	props: {
		onRender: (node: Element) => void
	};

	state: {
		ready: boolean
	};

	constructor(props) {
		super(props);

		this.state = {
			ready: false
		}
	}

	render() {
		return <div
			style={{ visibility: this.state.ready ? 'visible' : 'hidden' }}
			node={(node) => (this.setState({ready: true}) & this.props.onRender(node))}
		>
			{this.props.children}
		</div>;
	}

};