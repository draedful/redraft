import React from 'react';
import ReactDOM from 'react-dom';
import { applyMiddleware } from 'redux';
import perflogger from 'redux-perf-middleware';
import { Provider } from 'react-redux';

import createStore from 'core/redraft/createStore';

import EditorReducer from 'src/editor';

import Editor from './Editor';
import Menu from './Menu';

import './index.scss';

const store = createStore(EditorReducer, perflogger);

const reactContainer = document.createElement('div');
document.body.appendChild(reactContainer);

ReactDOM.render(
	(
		<Provider store={store} >
			<div className="custom-editor">
				<Menu />
				<Editor />
			</div>
		</Provider>
	),
	reactContainer
);