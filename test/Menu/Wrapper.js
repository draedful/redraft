// @flow
import type { SelectionState } from 'draft-js';
import { connect } from 'react-redux';
import { getSelectionRect, getSelection } from '../../src/src/editor/selectors';
import React from 'react';
import './Wrapper.scss';

const FloatingBoat = ({ top, left, children }) => (
	<div style={{position: 'absolute', left, top}}>
		{children}
	</div>
);

class FloatingMenu extends React.Component {

	props: {
		selection: SelectionState
	};

	state: {
		selectionPosition: ClientRect | null,
		floating: boolean
	};

	constructor(props) {
		super(props);
		this.state = {
			selectionPosition: null,
			floating: false
		}

	}

	componentWillReceiveProps(nextProps) {
		this.setState({
			selectionPosition: nextProps.selection.hasFocus && getSelectionRect(),
			floating: nextProps.selection.hasFocus
		})
	}

	render() {
		return <div ref={(node) => this.wrapper = node} className="custom-editor-menu">
			{
				this.state.floating ?
					<FloatingBoat {...this.state.selectionPosition}>
						{this.props.children}
					</FloatingBoat> :
					this.props.children
			}
		</div>
	}

}


export default connect((store) => ({ selection: getSelection(store) }))(FloatingMenu)