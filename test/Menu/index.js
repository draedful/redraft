import React from 'react';
import FloatingWrapper from './Wrapper';
import Menu from './Menu';

export default () => {
	return <FloatingWrapper>
		<Menu />
	</FloatingWrapper>
}