// @flow
import React from 'react';
import { connect } from 'react-redux';
import { toBold, toItalic } from 'src/styles/actions';

type MenuProps = {
	toBold: () => void,
	toItalic: () => void,
}

const Menu = ({ toBold, toItalic }: MenuProps) => (
	<div>
		<button onClick={toBold} onMouseDown={(e) => e.preventDefault()} >Bold</button>
		<button onClick={toItalic} onMouseDown={(e) => e.preventDefault()} >Italic</button>
	</div>
);


export default connect(void 0,
	(dispatch): MenuProps => ({
		toBold: () => dispatch(toBold()),
		toItalic: () => dispatch(toItalic()),
	})
)(Menu);